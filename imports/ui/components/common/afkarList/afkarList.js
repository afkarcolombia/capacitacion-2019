import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarList.html';
import './afkarList.less';



class AfkarList {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);

    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'afkarList';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  bindings: {
    twett: '='
  },
  controller: ['$reactive', '$scope', AfkarList],
  controllerAs: "ctrl"
});
