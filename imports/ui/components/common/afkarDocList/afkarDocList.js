import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarDocList.html';
import './afkarDocList.less';

import { name as AfkarList } from '../afkarList/afkarList'

// db
import { TwettsDB } from '/imports/api/twettsDB';
class AfkarDocList {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);
    var imagePath = 'afkar-mini.png';

    this.helpers({
      twetts(){
        return TwettsDB.find()
      }

    })
  }
  insertTwett(mensaje) {
    // console.log(mensaje)
    let obj =  {
      mensaje: mensaje
    }
    Meteor.call('insertTwett', obj, (err, res) => {
      if(err) alert(err)
    })
  }
}

const name = 'afkarDocList';

export default angular.module(name, [
  angularMeteor,
  AfkarList
  ])
.component(name, {
  template: template,
  controller: ['$reactive', '$scope', AfkarDocList],
  controllerAs: "ctrl"
});
