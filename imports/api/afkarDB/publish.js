import { Meteor } from 'meteor/meteor';

import { AfkarDB } from './collection';


if (Meteor.isServer) {
	Meteor.publish("afkarDB", function (query) {
		// console.log(query)
		if (query) {
			return AfkarDB.find(query.selector, query.options)
		}
		else{
			return AfkarDB.find();
		}
	});
}