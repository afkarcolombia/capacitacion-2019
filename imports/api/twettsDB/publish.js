import { Meteor } from 'meteor/meteor';

import { TwettsDB } from './collection';


if (Meteor.isServer) {
	Meteor.publish("twettsDB", function (query) {
		// console.log(query)
		if (query) {
			return TwettsDB.find(query.selector, query.options)
		}
		else{
			return TwettsDB.find();
		}
	});
}
