import { Mongo } from 'meteor/mongo';

export const TwettsDB = new Mongo.Collection("twettsDB");

TwettsDB.allow({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  }
});
